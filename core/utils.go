package core

import (
	"fmt"
	"gitlab.com/xiayesuifeng/go-i18n"
	"io/ioutil"
	"os"
	"os/exec"
	"os/user"
	"strconv"
)

func UserAdd(username, password string) error {
	pkgs := []string{"bumblebee", "wireshark", "tomcat7", "tomcat8", "sambashare"}

	groups := "wheel"
	for _, pkg := range pkgs {
		if SearchPackage(pkg) {
			groups += "," + pkg
		}
	}

	cmd := exec.Command("useradd", "-m", "-g", "users", "-G", groups, username)
	if err := cmd.Run(); err != nil {
		return err
	}

	cmd = exec.Command("bash", "-c", "echo \""+username+":"+password+"\" | chpasswd")
	if err := cmd.Run(); err != nil {
		return err
	}

	return nil
}

func SetHomeName(hostname string) error {
	return ioutil.WriteFile("/etc/hostname", []byte(hostname), 0644)
}

func SetLocale(username string) error {
	lang := i18n.GetLanguage()

	profile := "LANG DEFAULT=" + lang + ".UTF-8\nLANGUAGE DEFAULT=" + lang

	if lang != "en_US" {
		profile += ":en_US"
	}

	filename := "/home/" + username + "/.pam_environment"
	if err := ioutil.WriteFile(filename, []byte(profile), 0644); err != nil {
		return err
	}

	u, err := user.Lookup(username)
	if err != nil {
		return err
	}

	uid, err := strconv.Atoi(u.Uid)
	if err != nil {
		return err
	}

	gid, err := strconv.Atoi(u.Gid)
	if err != nil {
		return err
	}
	return os.Chown(filename, uid, gid)
}

func SetIM(username string) error {
	profile := `
GTK_IM_MODULE DEFAULT=%s
QT_IM_MODULE DEFAULT=%s
XMODIFIERS DEFAULT=\@im=%s
`
	if SearchPackage("fcitx5-git") || SearchPackage("fcitx5") {
		profile = fmt.Sprintf(profile, "fcitx5", "fcitx5", "fcitx")
	} else if SearchPackage("fcitx") {
		profile = fmt.Sprintf(profile, "fcitx", "fcitx", "fcitx")
	} else if SearchPackage("ibus") {
		profile = fmt.Sprintf(profile, "ibus", "ibus", "ibus")
	}

	f, err := os.OpenFile("/home/"+username+"/.pam_environment", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = f.WriteString(profile)
	return err
}

func DisableService() error {
	return exec.Command("systemctl", "disable", "firerain-firstboot").Run()
}

func SetWelcomeAutoStart(username string) error {
	if SearchPackage("firerain-welcome") {
		bytes,err := os.ReadFile("/usr/share/applications/firerain-welcome.desktop")
		if err != nil {
			return err
		}

		if err := os.MkdirAll("/home/"+username+"/.config/autostart",0755);err != nil {
			return err
		}

		if err := os.WriteFile("/home/"+username+"/.config/autostart/firerain-welcome.desktop",bytes,0644);err != nil {
			return err
		}

		u, err := user.Lookup(username)
		if err != nil {
			return err
		}

		uid, err := strconv.Atoi(u.Uid)
		if err != nil {
			return err
		}

		gid, err := strconv.Atoi(u.Gid)
		if err != nil {
			return err
		}

		if err := os.Chown("/home/"+username+"/.config", uid, gid);err != nil {
			return err
		}

		if err := os.Chown("/home/"+username+"/.config/autostart", uid, gid);err != nil {
			return err
		}

		return os.Chown("/home/"+username+"/.config/autostart/firerain-welcome.desktop", uid, gid)
	}

	return nil
}