package translations

import (
	"gitlab.com/xiayesuifeng/go-i18n"
)

func init() {
	i18n.AddTranslation(i18n.Translation{
		"Back":                              "返回",
		"Continue":                          "繼續",
		"OK":                                "確定",
		"Start using":                       "開始使用",
		"Create user account":               "創建用戶帳號",
		"Enter username and password":       "輸入用戶名和密碼",
		"Username":                          "用戶名",
		"Hostname":                          "主機名",
		"Password":                          "密碼",
		"Confirm user password":             "確認用戶密碼",
		"Username can not be empty":         "用戶名不能為空",
		"The hostname cannot be empty":      "主機名不能為空",
		"Password can not be blank":         "密碼不能為空",
		"Confirm password can not be blank": "確認密碼不能為空",
		"Confirm password does not match":   "確認密碼不匹配",
		"User creation failed, please switch to tty2 to create manually after clicking Start:\n": "用戶創建失敗,請在點擊開始使用後切換到tty2手動創建:\n",
		"The hostname setting failed, you can set it manually:\n":                                "主機名設置失敗,可手動設置:\n",
		"Locale setting failed, you can set it manually:\n":                                      "Locale設置失敗,可手動設置:\n",
		"The input method environment variable setting failed, you can set it manually:\n":       "輸入法環境變量設置失敗,可手動設置:\n",
		"Please wait...": "請稍等...",
		"Welcome":        "歡迎使用",
	}, "zh_HK")
}
